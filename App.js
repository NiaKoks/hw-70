import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import InputNumbButton from './InputNumbButton';

const btns =[
    ['C','DEL'],
  ['7','8','9','+'],
  ['4','5','6','-'],
  ['1','2','3','*'],
  ['0','.','=','/']
];
export default class App extends React.Component {
  constructor() {
  super()
  this.initialState ={
    displayValue:'0',
    operator: null,
    firstVal: '',
    secVal: '',
    nextVal: false,
  }
  this.state = this.initialState;
  }
  renderBtns(){
    let layout = btns.map((btnRows,index)=>{
      let rowItem = btnRows.map((btnItems,btnIndex)=>{
        return <InputNumbButton
            value={btnItems}
            handleOnPress={this.handleInput.bind(this,btnItems)}
             key={'btn-'+btnIndex}/>
            });
      return <View style={styles.inputRow} key={'row-'+index}>{rowItem}</View>
      });
    return layout
}

handleInput =(input)=>{
        const {displayValue, operator,firstVal,secVal,nextVal} =this.state;
        switch (input) {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':

            this.setState({
              displayValue: (displayValue === '0') ? input : displayValue + input
            })
              if (!nextVal){
                  this.setState({
                      firstVal: firstVal + input
                  })
              } else{
                  this.setState({
                      secVal: secVal + input
                  })
              }
            break;
          case '+':
          case '-':
          case '*':
          case '/':
            this.setState({
              nextVal: true,
              operator: input,
              displayValue: (operator !==null ? displayValue.substr(0,displayValue.length -1) : displayValue) + input
            })
            break;
          case '.':
            let dot = displayValue.toString().slice(-1)
            this.setState({
              displayValue: dot !== '.' ? displayValue + input : displayValue
            })
            if (!nextVal){
              this.setState({
                firstVal: firstVal + input
              })
            } else{
              this.setState({
                secVal: secVal + input
              })
            }
            break;
          case '=':
            let formatOperator = (operator == '*') ? '*' : (operator == '/') ? '/' : operator
            let result = eval(firstVal + formatOperator + secVal)
              this.setState({
                displayValue: result % 1 === 0 ? result :result.toFixed(2),
                firstVal: result % 1 === 0 ? result :result.toFixed(2),
                secVal: '',
                operator: null,
                nextVal: false
              })
            break;
          case 'C':
            this.setState(this.initialState);
            break;
          case 'DEL':
            let string = displayValue.toString();
            let delString = string.substr(0,string.length-1);
            let length = string.length;
            this.setState({
              displayValue: length == 1 ? '0' : delString,
              firstVal: length == 1 ? '0' : delString,
            })
            break;
        }
    }
render() {
  return (
      <View style={styles.container}>
        <View style={styles.resultContainer}>
          <Text style={styles.resultText}>
            {this.state.displayValue}
          </Text>
        </View>
        <View style={styles.inputContainer}>
          {this.renderBtns()}
        </View>
      </View>
  )}

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  resultContainer:{
    flex: 2,
    backgroundColor: '#D3D3D3'
    },
  inputContainer:{
    flex:8,
    backgroundColor: '#9E99AF'
  },
  resultText:{
    color: 'black',
    fontSize:60,
    fontWeight: 'bold',
    padding: 20,
    textAlign:'right'
  },
  inputRow:{
    flex: 1,
    flexDirection: 'row',
  }

});
